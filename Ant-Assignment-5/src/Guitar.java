
public class Guitar {
	
	private String guitarid;
	 private String guitarname;
	public String getGuitarid() {
		return guitarid;
	}
	public void setGuitarid(String guitarid) {
		this.guitarid = guitarid;
	}
	public String getGuitarname() {
		return guitarname;
	}
	public void setGuitarname(String guitarname) {
		this.guitarname = guitarname;
	}
  
	public void play(){
		
		System.out.println("Guitar playing in the background!!");
	}
}
